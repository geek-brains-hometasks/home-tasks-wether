package ru.lessons.asaprykin.hometask2.services;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.Locale;

import ru.lessons.asaprykin.hometask2.MainActivity;
import ru.lessons.asaprykin.hometask2.R;

public class NetLoaderService extends Service {
    private final static String SERVICE_LOADER_TAG = "NetLoaderService";
    private NetBinder binder = new NetBinder();
    private String[] cityNames;

    public NetLoaderService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void updateAllCityForecast(MainActivity currentActivity) {
        Log.d(SERVICE_LOADER_TAG, "Start update cities forecasts");
        Configuration configuration = new Configuration(currentActivity.getResources().getConfiguration());
        configuration.setLocale(new Locale("en"));
        this.cityNames = currentActivity.createConfigurationContext(configuration).getResources().getStringArray(R.array.city_selection);
        if (currentActivity != null) {
            for (String currentCity : cityNames) {
                currentActivity.updateWeatherData(currentCity);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class NetBinder extends Binder {
        public NetLoaderService getService() {
            return NetLoaderService.this;
        }
    }
}
