package ru.lessons.asaprykin.hometask2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.lessons.asaprykin.hometask2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PressureNextWeekFragment extends Fragment {

    @BindView(R.id.pressure_result) TextView pressureResult;
    @BindView(R.id.next_week_forecast_result) TextView nextWeekResult;

    private String pressure;
    private String nextWeek;

    public PressureNextWeekFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pressure_next_week, container, false);
        ButterKnife.bind(this, view);

        if (pressure != null)
            pressureResult.setText(pressure);
        if (nextWeek != null)
            nextWeekResult.setText(nextWeek);

        return view;
    }

    public void setData(String pressure, String nextWeek) {
        this.pressure = pressure;
        this.nextWeek = nextWeek;
    }

}
