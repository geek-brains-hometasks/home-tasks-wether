package ru.lessons.asaprykin.hometask2.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

/**
 * Created by andrejsaprykin on 27.05.2018.
 */

public class AppSettingsSp {
    private Context context;
    private SharedPreferences sharedPreferences;


    public AppSettingsSp(Context currentActivity) {
        this.context = currentActivity;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(currentActivity);
    }

    public void saveString(String key, String value) {
        sharedPreferences.edit().putString(key, encrypt(value)).apply();
    }

    public void saveInt(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void saveBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public String loadString(String key, String defValue) {
        String encryptVal = sharedPreferences.getString(key, null);
        if (encryptVal != null)
            return decrypt(encryptVal);
        else
            return defValue;
    }

    public int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    private String encrypt(String input) {
        // This is base64 encoding, which is not an encryption
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    private String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }
}
