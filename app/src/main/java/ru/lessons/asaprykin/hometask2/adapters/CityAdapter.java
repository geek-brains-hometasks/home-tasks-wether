package ru.lessons.asaprykin.hometask2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.lessons.asaprykin.hometask2.MainActivity;
import ru.lessons.asaprykin.hometask2.R;
import ru.lessons.asaprykin.hometask2.helpers.AppSettingsSp;
import ru.lessons.asaprykin.hometask2.interfaces.CityItemClick;

/**
 * Created by a.saprykin on 08.05.2018.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private Context context;
    private String[] cityList;
    private CityItemClick itemClick;
    private AppSettingsSp settingsSp;
    private int position;

    public CityAdapter(Context context) {
        this.context = context;
        this.cityList = context.getResources().getStringArray(R.array.city_selection);
        this.itemClick = ((MainActivity) context).getCityItemClick();
        settingsSp = new AppSettingsSp(context);
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_item, parent, false);
        return new CityViewHolder(item);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        holder.cityName.setText(cityList[position]);
    }

    @Override
    public int getItemCount() {
        return cityList.length;
    }


    public int getPosition() {
        return position;
    }

    class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener, ContextMenu.ContextMenuInfo {
        @BindView(R.id.city_name)
        TextView cityName;

        CityViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClick != null) {
                itemClick.onCityClick(getAdapterPosition());
                settingsSp.saveString(MainActivity.SAVE_LAST_CITY_KEY, cityName.getText().toString());
            }
        }


        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuInflater inflater = ((MainActivity) context).getMenuInflater();
            inflater.inflate(R.menu.list_context_menu, menu);
            position = getAdapterPosition();
        }


    }
}
