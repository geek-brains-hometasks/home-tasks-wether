package ru.lessons.asaprykin.hometask2.interfaces;

/**
 * Created by andrejsaprykin on 11.05.2018.
 */

public interface CityItemClick {
    void onCityClick(int position);
}
