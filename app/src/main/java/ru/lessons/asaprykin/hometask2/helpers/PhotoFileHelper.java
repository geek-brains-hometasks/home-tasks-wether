package ru.lessons.asaprykin.hometask2.helpers;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PhotoFileHelper {
    private final static String TAG = "PhotoFileHelperTag";

    private Context context;
    private boolean saveToExternalStorage;
    private String fileName;
    private String dirName;
    private Bitmap bitmapUserAvatar;
    private FileOutputStream outputStream;

    public PhotoFileHelper(Context context, boolean saveToExternalStorage) {
        this.context = context;
        this.saveToExternalStorage = saveToExternalStorage;
        this.fileName = "user_avatar.png";
        this.dirName = "ImageDir";
    }

    public void saveUserAvatar(ImageView developerAvatar) {
        bitmapUserAvatar = ((BitmapDrawable) developerAvatar.getDrawable()).getBitmap();
        try {
            outputStream = new FileOutputStream(getFile());
            bitmapUserAvatar.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.flush();
            Log.d(TAG, "Avatar saved!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void loadUserAvatar(ImageView developerAvatar) {
        File myPath = getFile();
        if (myPath.exists()) {
            Bitmap developerAva = BitmapFactory.decodeFile(myPath.getAbsolutePath());
            Log.d(TAG, "Avatar loaded: " + developerAva);
            developerAvatar.setImageBitmap(developerAva);
        }
    }

    private File getFile() {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File dir;
        if (!saveToExternalStorage) {
            dir = cw.getDir(dirName, Context.MODE_PRIVATE);
        } else {
            dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WeatherApp/" + dirName + "/");
            if (!dir.exists()) {
                dir.mkdirs();
            }
        }
        return new File(dir, fileName);
    }
}
