package ru.lessons.asaprykin.hometask2;


import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.lessons.asaprykin.hometask2.fragments.CityListFragment;
import ru.lessons.asaprykin.hometask2.fragments.WeatherForecastFragment;
import ru.lessons.asaprykin.hometask2.helpers.AppSettingsSp;
import ru.lessons.asaprykin.hometask2.helpers.DatabaseHelper;
import ru.lessons.asaprykin.hometask2.helpers.ForecastHelper;
import ru.lessons.asaprykin.hometask2.helpers.PhotoFileHelper;
import ru.lessons.asaprykin.hometask2.helpers.WeatherLoadTask;
import ru.lessons.asaprykin.hometask2.interfaces.CityItemClick;
import ru.lessons.asaprykin.hometask2.services.NetLoaderService;

public class MainActivity extends AppCompatActivity {
    public static final String WEATHER_TAG = "WEATHER_TAG";

    public static final String RESULT_SHARED_KEY = "ru.lessons.shared_key";
    public final static String SAVE_LAST_CITY_KEY = "ru.lessons.asaprykin.last_chosen_city";
    public static int REQUEST_SHARE_CODE = 51128;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navView;
    ImageView avatar;
    private FragmentTransaction fragmentTransaction;

    private CityListFragment cityListFragment;
    private WeatherForecastFragment weatherForecastFragment;
    private CityItemClick cityItemClick;

    private ForecastHelper forecastHelper;
    private AppSettingsSp settingsSp;
    private PhotoFileHelper photoFileHelper;
    private DatabaseHelper databaseHelper;

    private ServiceConnection serviceConnection;
    private NetLoaderService loaderService;
    private boolean serviceBind = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showLog("onCreate");

        ButterKnife.bind(this);

        initHelpers();
        initViews();
        initFragments();
        checkLastCity();
        initService();
        onBindService();

        cityItemClick = new CityItemClick() {
            @Override
            public void onCityClick(int position) {
                forecastHelper.getForecast(position);
                databaseHelper.addForecast(forecastHelper.getCurrentCity(), forecastHelper.getCurrentForecast());
                setWeather(forecastHelper.getCurrentForecast(),
                        forecastHelper.getCurrentPressure(),
                        forecastHelper.getCurrentNextWeek(),
                        forecastHelper.getCurrentWeatherIcon());
            }
        };

        showLog("onCreate");
    }

    private void initService() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(WEATHER_TAG, "Start connect to service NetLoader");
                loaderService = ((NetLoaderService.NetBinder) service).getService();
                loaderService.updateAllCityForecast(MainActivity.this);
                serviceBind = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(WEATHER_TAG, "Disconnect from NetLoader service");
                serviceBind = false;
            }
        };
    }

    private void onBindService() {
        Intent serviceIntent = new Intent(getBaseContext(), NetLoaderService.class);
        if (serviceConnection != null) {
            bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
            Log.d(WEATHER_TAG, "NetLoaderService was bind");
        }
    }


    private void onUnbindService() {
        if (serviceBind) {
            unbindService(serviceConnection);
            serviceBind = false;
        }
    }

    private void initHelpers() {
        forecastHelper = new ForecastHelper(this);
        settingsSp = new AppSettingsSp(this);
        photoFileHelper = new PhotoFileHelper(this, false);
        databaseHelper = new DatabaseHelper(this);
    }

    private void initViews() {
        View navHeader = navView.getHeaderView(0);
        avatar = navHeader.findViewById(R.id.developer_avatar);
        if (photoFileHelper != null) {
            photoFileHelper.loadUserAvatar(avatar);
        }
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                drawerLayout.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.nav_about:
                        showToastMessage("Click navigation About");
                        return true;
                    case R.id.nav_save_photo:
                        photoFileHelper.saveUserAvatar(avatar);
                        showToastMessage("Click save photo");
                        return true;
                    case R.id.nav_feedback:
                        showToastMessage("Click navigation Feedback");
                        return true;
                    case R.id.nav_add_reminder:
                        showToastMessage("Click navigation Reminder");
                        return true;
                    case R.id.nav_share_app:
                        showToastMessage("Click navigation Share");
                        return true;
                }
                return false;
            }
        });
    }

    private void initFragments() {
        cityListFragment = new CityListFragment();
        weatherForecastFragment = new WeatherForecastFragment();
        setCurrentFragment(cityListFragment);
    }


    private void setCurrentFragment(Fragment currentFragment) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, currentFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void showLog(String message) {
        Log.d(WEATHER_TAG, message);
    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public CityItemClick getCityItemClick() {
        return cityItemClick;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            databaseHelper.close();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        showLog("onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showLog("onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLog("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        showLog("onPause");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainActivity.REQUEST_SHARE_CODE) {
            if (resultCode == RESULT_OK) {
                showLog(data.getStringExtra(RESULT_SHARED_KEY));
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        showLog("onSaveInstanceState");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_weather_type_id:
                showToastMessage("Weather type stub!");
                return true;
            case R.id.menu_weather_real_feel_id:
                showToastMessage("Weather real feel stub");
                return true;
            case R.id.menu_weather_about_id:
                showToastMessage(getString(R.string.info_about_app));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        onUnbindService();
        showLog("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showLog("onDestroy");
    }

    public void updateWeatherData(String cityName) {
        new WeatherLoadTask(cityName, this, databaseHelper).execute();
    }

    public void setWeather(String forecast, String pressure, String nextWeek, int icon) {
        weatherForecastFragment.setData(forecast,
                pressure,
                nextWeek,
                icon);
        setCurrentFragment(weatherForecastFragment);
    }

    private int getPositionByCityName(String[] cityList, String cityName) {
        return Arrays.asList(cityList).indexOf(cityName);
    }

    private void checkLastCity() {
        String lastCity = settingsSp.loadString(SAVE_LAST_CITY_KEY, null);
        if (lastCity != null) {
            int position = getPositionByCityName(getResources().getStringArray(R.array.city_selection), lastCity);
            if (position != -1) {
                forecastHelper.getForecast(position);
                setWeather(forecastHelper.getCurrentForecast(),
                        forecastHelper.getCurrentPressure(),
                        forecastHelper.getCurrentNextWeek(),
                        forecastHelper.getCurrentWeatherIcon());
            }
        }
    }
}
