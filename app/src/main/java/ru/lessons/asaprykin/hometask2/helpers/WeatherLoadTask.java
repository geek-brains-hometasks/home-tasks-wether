package ru.lessons.asaprykin.hometask2.helpers;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import ru.lessons.asaprykin.hometask2.MainActivity;

public class WeatherLoadTask extends AsyncTask<Void, Void, JSONObject> {

    private String cityName;
    private Context context;
    private DatabaseHelper databaseHelper;

    public WeatherLoadTask(String cityName, Context context, DatabaseHelper databaseHelper) {
        this.cityName = cityName;
        this.context = context;
        this.databaseHelper = databaseHelper;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        return WeatherLoader.getJSONData(context, cityName);
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if (jsonObject == null) {
            Toast.makeText(context, "NotFound", Toast.LENGTH_SHORT).show();
        } else {
            try {
                JSONObject weather = jsonObject.getJSONObject("weather");
                JSONObject main = jsonObject.getJSONObject("main");

                databaseHelper.addForecast(cityName, weather.getString("main"));

                ((MainActivity)context).setWeather(weather.getString("main"), main.getString("pressure"), "No data", -1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
