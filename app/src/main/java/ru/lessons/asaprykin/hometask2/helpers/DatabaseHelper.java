package ru.lessons.asaprykin.hometask2.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "city_weather.db";
    private static final int DATABASE_VERSION = 1;

    private final String TABLE_FORECAST = "city_forecast";

    public final String COLUMN_ID = "_id";
    public final String COLUMN_CITY = "city_name";
    public final String COLUMN_FORECAST = "city_forecast";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_FORECAST + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_CITY + " TEXT, " + COLUMN_FORECAST + " TEXT);");
        Log.d(DB_TAG, "DB created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST);
        onCreate(db);
    }

    public void addForecast(String cityName, String forecast) {
        if (!updateForecast(cityName, forecast)) {
            insertForecast(cityName, forecast);
        }
    }


    private void insertForecast(String cityName, String forecast) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY, cityName);
        values.put(COLUMN_FORECAST, forecast);

        database.insert(TABLE_FORECAST, null, values);
        Log.d(DB_TAG, "insert new forecast data");
    }


    /**
     * Не учитываем что имена городов могут совпадать.
     * Да того, что бы избежать подобной ситуации необходимо
     * вводить дополнительные параметры или делать уникальной колнку с названием города
     */
    private boolean updateForecast(String cityName, String forecast) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_FORECAST, forecast);

        String selection = COLUMN_CITY + " =?";
        String[] selectionArgs = {cityName};

        int count = database.update(TABLE_FORECAST, values, selection, selectionArgs);
        Log.d(DB_TAG, "Updated " + count + " columns");
        return count > 0;
    }

}
