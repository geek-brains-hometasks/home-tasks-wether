package ru.lessons.asaprykin.hometask2.helpers;

import android.content.Context;
import android.content.res.TypedArray;

import ru.lessons.asaprykin.hometask2.R;

/**
 * Created by a.saprykin on 24.04.2018.
 */

public class ForecastHelper {
    private String[] cityNames;
    private String[] forecasts;
    private String[] pressures;
    private String[] forecastsNextWeek;
    private TypedArray forecastIcons;

    private String currentCity;
    private String currentForecast;
    private String currentPressure;
    private String currentNextWeek;
    private int currentWeatherIcon;

    private String notFound;

    public ForecastHelper(Context context) {
        this.cityNames = context.getResources().getStringArray(R.array.city_selection);
        this.forecasts = context.getResources().getStringArray(R.array.city_weather_forecast);
        this.pressures = context.getResources().getStringArray(R.array.city_weather_pressure);
        this.forecastsNextWeek = context.getResources().getStringArray(R.array.city_weather_next_week);
        this.forecastIcons = context.getResources().obtainTypedArray(R.array.forecast_icons);
        this.notFound = context.getResources().getString(R.string.forecast_not_found);
    }

    public void getForecast(int position) {
        if (this.forecasts == null) {
            currentForecast = notFound;
        } else {
            currentForecast = forecasts[position];
        }

        if (this.pressures == null) {
            currentPressure = "";
        } else {
            currentPressure = pressures[position];
        }

        if (this.forecastsNextWeek == null) {
            currentNextWeek = "";
        } else {
            currentNextWeek = forecastsNextWeek[position];
        }

        if (this.cityNames != null) {
            currentCity = cityNames[position];
        }

        currentWeatherIcon = forecastIcons.getResourceId(position, -1);
    }

    public String getCurrentCity() {
        return currentCity;
    }
    
    public String getCurrentForecast() {
        return currentForecast;
    }

    public String getCurrentPressure() {
        return currentPressure;
    }

    public String getCurrentNextWeek() {
        return currentNextWeek;
    }

    public int getCurrentWeatherIcon() {
        return currentWeatherIcon;
    }
}
