package ru.lessons.asaprykin.hometask2.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.lessons.asaprykin.hometask2.MainActivity;
import ru.lessons.asaprykin.hometask2.R;
import ru.lessons.asaprykin.hometask2.adapters.CityAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityListFragment extends Fragment {

    @BindView(R.id.city_list_rv)
    RecyclerView cityListRv;
    @BindView(R.id.support_button)
    Button supportBtn;
    @BindView(R.id.other_city_fab)
    FloatingActionButton otherCityFab;

    private CityAdapter cityAdapter;

    public CityListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_city_list, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        cityListRv.setLayoutManager(layoutManager);
        cityListRv.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        cityAdapter = new CityAdapter(getActivity());
        cityListRv.setAdapter(cityAdapter);

        supportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp(v);
            }
        });
        otherCityFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCityNameDialog();
            }
        });
    }

    private void showCityNameDialog() {
        final AlertDialog.Builder cityInput = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_city_name, null);
        final TextInputEditText editText = dialogView.findViewById(R.id.edit_city_name);
        cityInput.setView(dialogView);
        cityInput.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String cityName = editText.getText().toString();
                if (!cityName.isEmpty())
                    ((MainActivity) getActivity()).updateWeatherData(cityName);
            }
        });
        cityInput.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        cityInput.show();
    }

    private void showPopUp(View v) {
        PopupMenu popupMenu = new PopupMenu(getActivity(), v);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_send_message_id:
                        showToastMessage("Send message to support stub!");
                        return true;
                    case R.id.menu_send_feedback_id:
                        showToastMessage("Send feedback stub!");
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.inflate(R.menu.support_pop_up_menu);
        popupMenu.show();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = cityAdapter.getPosition();
        switch (item.getItemId()) {
            case R.id.menu_show_month_id:
                showToastMessage("Month stub!!! position: " + position);
                return true;
            case R.id.menu_show_previous_id:
                showToastMessage("Previous stub!!! position: " + position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
