package ru.lessons.asaprykin.hometask2.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.lessons.asaprykin.hometask2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherForecastFragment extends Fragment {
    private final static String PRESSURE_NEXT_WEEK_FRAGMENT_TAG = "ru.lessons.asaprykin.hometask2.fragments_pressure_next_week_fragment_tag";

    @BindView(R.id.forecast_text)
    TextView forecastText;
    @BindView(R.id.share_fab)
    FloatingActionButton shareButton;
    @BindView(R.id.forecast_image)
    ImageView forecastImage;

    private String forecast;
    private String pressure;
    private String nextWeek;
    private int forecastIcon = -1;

    private PressureNextWeekFragment pressureNextWeekFragment;

    public WeatherForecastFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_forecast, container, false);
        ButterKnife.bind(this, view);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareForecast(Intent.ACTION_SEND);
            }
        });
        if (forecast != null)
            forecastText.setText(forecast);
        if (forecastIcon != -1)
            forecastImage.setImageDrawable(getActivity().getResources().getDrawable(forecastIcon));
        prepareAndShowInnerFragment();
        return view;
    }


    public void setData(String forecast, String pressure, String nextWeek, int icon) {
        if (forecast != null && !forecast.isEmpty())
            this.forecast = forecast;

        if (pressure != null && !pressure.isEmpty())
            this.pressure = pressure;

        if (nextWeek != null && !nextWeek.isEmpty())
            this.nextWeek = nextWeek;

        this.forecastIcon = icon;
    }

    private void shareForecast(String action) {
        Intent shareIntent = new Intent(action);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, forecastText.getText().toString());
        if (getActivity().getPackageManager().queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0)
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_title)));
    }

    private void prepareAndShowInnerFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();
        pressureNextWeekFragment = (PressureNextWeekFragment) fragmentManager.findFragmentByTag(PRESSURE_NEXT_WEEK_FRAGMENT_TAG);
        if (pressureNextWeekFragment == null)
            pressureNextWeekFragment = new PressureNextWeekFragment();

        if (pressure != null && nextWeek != null)
            pressureNextWeekFragment.setData(pressure, nextWeek);

        fragmentManager.beginTransaction().replace(R.id.forecast_fragment_container, pressureNextWeekFragment, PRESSURE_NEXT_WEEK_FRAGMENT_TAG).commit();

    }
}
